package br.com.m2msolutions.sinotico;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.m2msolutions.sinotico.adapter.LineCustomAdapter;
import br.com.m2msolutions.sinotico.adapter.LineCustomRowHolderInfo;
import br.com.m2msolutions.sinotico.cloud.BusserviceService;
import br.com.m2msolutions.sinotico.cloud.ControlPointService;
import br.com.m2msolutions.sinotico.cloud.VehicleService;
import br.com.m2msolutions.sinotico.model.BusService;
import br.com.m2msolutions.sinotico.model.ControlPoint;
import br.com.m2msolutions.sinotico.model.PatternType;
import br.com.m2msolutions.sinotico.model.Vehicle;
import br.com.m2msolutions.sinotico.util.AppHelper;
import br.com.m2msolutions.sinotico.util.LogUtil;
import br.com.m2msolutions.sinotico.views.ControPointSinotipo;
import br.com.m2msolutions.sinotico.views.ControlPointSelector;
import br.com.m2msolutions.sinotico.views.LineSinotipo;
import br.com.m2msolutions.sinotico.views.VehicleSinotipo;

@SuppressWarnings("deprecation")
public class MainActivity extends RoboActivity implements OnTouchListener{

	private static SearchBusServiceTask busServiceSearchTask;
	private static SearchTask searchTask;

	public static final int GROW = 0;
	public static final int SHRINK = 1;
	public static final int DURATION = 150;

	public static final float MIN_SCALE = 1f;
	public static final float MAX_SCALE = 2.5f;

	protected float x1, x2, y1, y2, x1_pre, y1_pre, x_scale = 1.0f,
			y_scale = 1.0f, dist_curr = -1, dist_pre = -1, dist_delta,zoom = 0;

	private long mLastGestureTime;
	//	private Integer height;
	private int sensibility = 10;
	private int lineHeight;
	private int lineX;

	//	private int finalControlPointVehicleCount;
	//	private int outgoingVechicleCount;
	//	private int returnVechicleCount;
	//	private int outOfRouteVehicleCount;

	private List<Vehicle> vehicles;
	private List<ControlPoint> controlPoints;
	private List<ControPointSinotipo> pointsSinotico;
	private List<BusService> busServices;
	private String[] lines;
	private BusService busService;


	//	private Vehicle nextOutgoingVehicle;
	//	private Vehicle nextReturningVehicle;
	private boolean outgoing = true;
	List<String> lineNames = new ArrayList<String>();
	List<String> linePosition = new ArrayList<String>();

	@InjectView(R.id.view_sinotico) FrameLayout frameLayout;
	@InjectView(R.id.autocomplete_line)	AutoCompleteTextView autoCompleteLines;
	@InjectView(R.id.initialMarker) Button initialMarker; 
	@InjectView(R.id.finalMarker) Button finalMarker;
	@InjectView(R.id.statusBar) LinearLayout statusBar;
	@InjectView(R.id.header_container) LinearLayout headerContainer;
	@InjectView(R.id.scrollView) ScrollView scrollView;
	private RefreshVehicleTask refreshTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LogUtil.initLog(this, getString(R.string.app_name));
		startActivity(new Intent(this,SplashScreenActivity.class));
		setContentView(R.layout.activity_main);
		frameLayout.setOnClickListener(onClickListener);
		autoCompleteLines.setOnItemClickListener(itemListener);
		populateLines();
	}

	private void setAutoCompleteAdapter() {
		if (busServices == null)
			return;

		lines = new String[busServices.size()];
		int count = 0;
		for (BusService busServ : busServices) {
			lines[count] = busServ.getName();
			count++;
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, lines);
		autoCompleteLines.setAdapter(adapter);
	}

	private void populateLines() {
//		busServiceSearchTask = new SearchBusServiceTask(this);
//		busServiceSearchTask.execute();
		generateMockLine();
		setAutoCompleteAdapter();
	}

	private void populateLine(){
//		searchTask = new SearchTask(this);
//		searchTask.execute();
		generateMockPoints();
		generateMockVehicles();
		drawLine();
	}

	private void refreshVehicles(){
//		refreshTask = new RefreshVehicleTask(this);
//		refreshTask.execute();
		generateMockVehicles();
		drawLine();
	}

	private void drawLine() {
		LogUtil.i("adicionando componentes visiuais na tela");
		pointsSinotico = new ArrayList<ControPointSinotipo>();
		getOnGoingAndOutGoingLabels();
		getSinoticoBounds();
		setViewBounds();
		frameLayout.removeAllViews();
		addSelectedPatternLine();
		addControlPoints();
		addVehicles();
		frameLayout.invalidate();
	}

	private void setViewBounds() {
		LayoutParams params = new LayoutParams(frameLayout.getWidth(), lineHeight);
		frameLayout.setLayoutParams(params);
	}

	private int getWidth(){
		int width = getWindow().getDecorView().getWidth();
		return width;
	}


	private int getHeigth() {
		int heigth = getWindow().getDecorView().getHeight();
		return heigth - headerContainer.getHeight();
	}

	private void getOnGoingAndOutGoingLabels() {
		LogUtil.i("colocando as labels nos botões e tornando os visíveis");
		getInitialPoint();
		getEndingPoint();
		statusBar.setVisibility(View.VISIBLE);
	}

	private void getInitialPoint() {
		if(controlPoints!= null && !controlPoints.isEmpty()){
			for(ControlPoint controlPoint : controlPoints){
				if(controlPoint.getPatternFraction() == 0 ){
					initialMarker.setText(trimStringToButton(controlPoint.getName()));
					return;
				}
			}
		}

	}

	private void getEndingPoint() {
		if(controlPoints!= null && !controlPoints.isEmpty()){
			for(ControlPoint controlPoint : controlPoints){
				if(controlPoint.getPatternFraction() == 1 ){
					finalMarker.setText(trimStringToButton(controlPoint.getName()));
					return;
				}
			}
		}
	}

	private CharSequence trimStringToButton(String name) {
		if(name.length() > 10){
			return name.substring(0, 3)+getString(R.string.triple_dots)+name.substring(name.length()-4, name.length());
		}
		return name;
	}

	private void addSelectedPatternLine(){
		if(outgoing)
			addOutgoingLine();
		else
			addReturningLine();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		return super.dispatchTouchEvent(ev);
	}

	private void addOutgoingLine() {
		frameLayout.addView(new LineSinotipo(this, lineX, 0,lineX,lineHeight));
	}

	private void addReturningLine() {
		frameLayout.addView(new LineSinotipo(this, lineX, 0,lineX,lineHeight));
	}

	private void addControlPoints() {
		if (controlPoints == null) {
			return;
		}

		for (int i = 0; i < controlPoints.size(); i++) {
			ControlPoint controlPoint = controlPoints.get(i);
			
			if (controlPoint.getPatternType() == null) {
				continue;
			}
			if ((controlPoint.getPatternType() == PatternType.OUTGOING && outgoing) || (controlPoint.getPatternType() == PatternType.RETURNING && !outgoing)) {
				ControPointSinotipo pointSinotipo = new ControPointSinotipo(this, lineX, getY(controlPoint.getPatternFraction(), controlPoint.getPatternType()), controlPoint);
				pointSinotipo.setTag(controlPoint);
				pointsSinotico.add(pointSinotipo);
			}
		}
		
		drawallPoints();
	}

	private void drawallPoints() {
		for(ControPointSinotipo pointSinotipo :pointsSinotico){
			frameLayout.addView(pointSinotipo);
		}
	}

	private void addVehicles() {
		if (vehicles == null) {
			return;
		}
		for (Vehicle vehicle : vehicles) {
			if (vehicle.getPatternType() == null) {
				continue;
			}
			if ((vehicle.getPatternTypeEnumValue() == PatternType.OUTGOING && outgoing) || 
					(vehicle.getPatternTypeEnumValue() == PatternType.RETURNING && !outgoing)) {
				VehicleSinotipo vehicleSinotipo = new VehicleSinotipo(this, lineX, getY(vehicle.getPatternFraction(), PatternType.getType(vehicle.getPatternType())), vehicle);
				frameLayout.addView(vehicleSinotipo);
			}

		}
	}

	private int getY(double fraction, PatternType patternType) {
		int y = (int) (lineHeight * fraction);
		if(fraction == 1){
			y = y - 60;			
			return y;
		} 
//		if(fraction == 0 && patternType == PatternType.RETURNING){
//			y = lineHeight - y -60;
//			return y;
//		}
//		if (patternType == PatternType.RETURNING) {
//			y = lineHeight - y;
//		}
		return y;
	}

	private void getSinoticoBounds() {
		LogUtil.i("pegando as margens do sinótico");
		lineX = getWidth()/4;
		lineHeight = (int) (getHeigth()+(zoom * sensibility));
	}


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return false;
	}


	// BUS SERVICE TASK

	private class SearchBusServiceTask extends
	AsyncTask<String, Void, List<BusService>> {
		Context context;
		String erroMessage = null;
		ProgressDialog mDialog;

		SearchBusServiceTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "",
					context.getString(R.string.buscando_linhas), true, false);
			mDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			statusBar.setVisibility(View.GONE);
			setProgressBarIndeterminate(true);
		}

		@Override
		public List<BusService> doInBackground(String... params) {
			return search();
		}

		List<BusService> search() {
			BusserviceService service = new BusserviceService();
			try {
				service.getAllBusServices(context);
			} catch (Exception e) {
				erroMessage = e.getLocalizedMessage();
				return null;
			}
			if (service.getResponseCode() != 200) {
				erroMessage = service.getExceptionConstForMessage();
				return null;
			}

			try {
				return service.getBusServiceListFromResponse();
			} catch (JSONException e) {
				erroMessage = e.getLocalizedMessage();
			}
			return null;
		}

		@Override
		public void onPostExecute(List<BusService> busServicesResult) {
			if(MainActivity.this == null)
				return;
			busServiceSearchTask = null;
			setProgressBarIndeterminate(false);
			mDialog.dismiss();

			if (busServicesResult != null) {
				if (busServicesResult.size() > 0) {
					busServices = busServicesResult;
				} else {
					AppHelper.getInstance().presentError(
							context,
							getResources().getString(R.string.NOT_FOUND_TITLE),
							getResources()
							.getString(R.string.NOT_FOUND_MESSAGE));
				}
			} else {
				if (erroMessage != null) {
					AppHelper.getInstance()
					.presentError(
							context,
							getResources().getString(
									R.string.ERROR_CONNECTION),
									erroMessage);

				}
			}
			setAutoCompleteAdapter();
		}
	}

	// SEARH SINOTICO DATA TASK

	private class SearchTask extends AsyncTask<String, Void, Boolean> {
		Context context;
		String vehicleErroMessage = null;
		String controlPointErroMessage = null;
		ProgressDialog mDialog;

		SearchTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "",
					context.getString(R.string.buscando_paradas_e_veiculos), true, false);
			mDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			setProgressBarIndeterminate(true);
		}

		@Override
		public Boolean doInBackground(String... params) {
			search();
			return true;
		}

		private void search() {
			getVehicles();
			getControlPoints();
		}

		private void getVehicles() {
			VehicleService vehicleService = new VehicleService();
			try {
				vehicleService.getVehiclesByBusService(
						busService.getBusServiceId(), context);
			} catch (Exception e) {
				vehicleErroMessage = e.getLocalizedMessage();
				return;
			}
			if (vehicleService.getResponseCode() != 200) {
				vehicleErroMessage = vehicleService
						.getExceptionConstForMessage();
				return;
			}

			try {
				vehicles = vehicleService.getVehicleListFromResponse();
				LogUtil.i("recebido os véiculos");
			} catch (JSONException e) {
				vehicleErroMessage = e.getLocalizedMessage();
				return;
			} catch (ParseException e) {
				vehicleErroMessage = e.getLocalizedMessage();
			}
		}

		private void getControlPoints() {
			LogUtil.i("fazendo a chamada das paradas");
			ControlPointService controlPointservice = new ControlPointService();
			try {
				controlPointservice.getControlPointsByBusService(
						busService.getBusServiceId(), context);
			} catch (Exception e) {
				controlPointErroMessage = e.getLocalizedMessage();
				return;
			}
			if (controlPointservice.getResponseCode() != 200) {
				controlPointErroMessage = controlPointservice
						.getExceptionConstForMessage();
				return;
			}

			try {
				controlPoints = controlPointservice
						.getControlPointListFromResponse();
				LogUtil.i("Paradas recebidas");
			} catch (JSONException e) {
				controlPointErroMessage = e.getLocalizedMessage();
				return;
			} catch (ParseException e) {
				controlPointErroMessage = e.getLocalizedMessage();
			}
		}

		@Override
		public void onPostExecute(Boolean result) {
			if(MainActivity.this == null)
				return;
			searchTask = null;
			setProgressBarIndeterminate(false);
			mDialog.cancel();

			if (vehicleErroMessage != null) {
				AppHelper.getInstance().presentError(context,
						getResources().getString(R.string.ERROR_CONNECTION),
						vehicleErroMessage);
				return;
			}

			if (controlPointErroMessage != null) {
				AppHelper.getInstance().presentError(context,
						getResources().getString(R.string.ERROR_CONNECTION),
						controlPointErroMessage);
				return;
			}
			drawLine();
		}
	}
	private class RefreshVehicleTask extends AsyncTask<String, Void, Boolean> {
		Context context;
		String vehicleErroMessage = null;
		String controlPointErroMessage = null;
		ProgressDialog mDialog;

		public RefreshVehicleTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "",context.getString(R.string.atualizando_veiculos), true, false);
			mDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			setProgressBarIndeterminate(true);
		}

		@Override
		public Boolean doInBackground(String... params) {
			search();
			return true;
		}

		private void search() {
			getVehicles();
		}

		private void getVehicles() {
			VehicleService vehicleService = new VehicleService();
			try {
				vehicleService.getVehiclesByBusService(
						busService.getBusServiceId(), context);
			} catch (Exception e) {
				vehicleErroMessage = e.getLocalizedMessage();
				return;
			}
			if (vehicleService.getResponseCode() != 200) {
				vehicleErroMessage = vehicleService
						.getExceptionConstForMessage();
				return;
			}

			try {
				vehicles = vehicleService.getVehicleListFromResponse();
				LogUtil.i("recebido os véiculos");
			} catch (JSONException e) {
				vehicleErroMessage = e.getLocalizedMessage();
				return;
			} catch (ParseException e) {
				vehicleErroMessage = e.getLocalizedMessage();
			}
		}

		@Override
		public void onPostExecute(Boolean result) {
			if(MainActivity.this == null)
				return;
			searchTask = null;
			setProgressBarIndeterminate(false);
			mDialog.cancel();

			if (vehicleErroMessage != null) {
				AppHelper.getInstance().presentError(context,
						getResources().getString(R.string.ERROR_CONNECTION),
						vehicleErroMessage);
				return;
			}

			if (controlPointErroMessage != null) {
				AppHelper.getInstance().presentError(context,
						getResources().getString(R.string.ERROR_CONNECTION),
						controlPointErroMessage);
				return;
			}
			drawLine();
		}
	}

	public void onClickRefresh(View view){
		if(busService != null && (controlPoints == null || controlPoints.isEmpty())){
			populateLine();
			return;
		}
		if(controlPoints != null && !controlPoints.isEmpty()){
			refreshVehicles();
			return;
		}

		populateLines();

	}

	public void onClickOutgoing(View view){
		if(outgoing){
			return;
		}
		outgoing = true;
		initialMarker.setBackgroundResource(R.drawable.btnmenubaryellow);
		finalMarker.setBackgroundResource(R.drawable.btnmenubar);
		drawLine();

	}

	public void onClickReturning(View view){
		if(!outgoing){
			return;
		}
		outgoing = false;
		initialMarker.setBackgroundResource(R.drawable.btnmenubar);
		finalMarker.setBackgroundResource(R.drawable.btnmenubaryellow);
		drawLine();
	}




	//Listeners

	private final OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			long now = System.currentTimeMillis();
			//			if(now - mLastGestureTime < 400){
			//				LogUtil.d("double click");
			//				LogUtil.d("valor do zoom: "+zoom);
			//				LogUtil.d("valor da altura: "+lineHeight);
			//				mLastGestureTime = now;
			//				zoomIn();
			//			}else{
			//				LogUtil.d("single click");
			//				LogUtil.d("valor do zoom: "+zoom);
			//				LogUtil.d("valor da altura: "+lineHeight);
			//				mLastGestureTime = now;
			//				zoomOut();				
			//			}
		}
	};


	public void zoomInButtonClick(View view){
		zoomIn();
	}
	
	public void zoomOutButtonClick(View view){
		zoomOut();
	}
	
	private void zoomOut() {
		if(zoom == 0){
			//DO Noting
		}else if(zoom == 2 || zoom == 1){
			zoom = 0;
		}else{
			zoom = zoom/2;
		}
		drawLine();
	}

	private void zoomIn() {
		if(zoom == 0){
			zoom = 2;
		}else{
			zoom = zoom*2;
		}
		drawLine();
	}


	private final OnItemClickListener itemListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
				long arg3) {
			busService = busServices.get(pos);
			populateLine();

		}

	};

	public void onClickSearch(View view){
		search();
	}
	
	private void search(){
		if(lines == null || lines.length == 0){
			AppHelper.getInstance().presentError(MainActivity.this, getString(R.string.atencao), getString(R.string.n_o_foram_baixadas_linhas_por_favor_usar_o_bot_o_de_atualizar));
			return;
		}
		
		String pattern = autoCompleteLines.getText().toString();
		lineNames.clear();
		linePosition.clear();
		for (int pos = 0; pos < lines.length; pos++) {
			if(lines[pos].toUpperCase().contains(pattern.toUpperCase())){
				lineNames.add(lines[pos]);
				linePosition.add(Integer.toString(pos));
			}
		}

		if(lineNames.isEmpty() || linePosition.isEmpty()){
			AppHelper.getInstance().presentError(MainActivity.this, getString(R.string.atencao), getString(R.string.sem_linhas_pesquisa));
			return;
		}

		setDialogWithLines();
	}
	
	public void onClickClean(View view){
		autoCompleteLines.setText("");
	}

	private void setDialogWithLines() {
		final  Dialog dialog = new Dialog(this);
		dialog.setTitle(R.string.linhas_encontradas);
		dialog.setContentView(R.layout.list_search_dialog);
		final Button button = (Button) dialog.findViewById(R.id.cancelButton);
		final ListView listView = (ListView) dialog.findViewById(R.id.searchedList);
		button.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();	
			}
		});

		listView.setAdapter(new LineCustomAdapter(this, lineNames, linePosition));
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,long arg3) {
				LineCustomRowHolderInfo rowInfo = (LineCustomRowHolderInfo)	arg1.getTag();
				autoCompleteLines.setText(rowInfo.lineName.getText());
				busService = busServices.get(Integer.parseInt(rowInfo.linePosition.getText().toString()));
				populateLine();
				lineNames.clear();
				linePosition.clear();
				dialog.cancel();
			}
		});
		dialog.show();

	}

	private void generateMockLine(){
		BusService busService = new BusService();
		busService.setName("120 - Linha teste");
		busService.setBusServiceId((long)1);
		busService.setShortName("120 -LT");
		busServices = new ArrayList<BusService>();
		busServices.add(busService);
	}


	private void generateMockPoints(){
		ControlPoint point = new ControlPoint();
		ControlPoint point1 = new ControlPoint();
		ControlPoint point2 = new ControlPoint();
		ControlPoint point3 = new ControlPoint();
		ControlPoint point4 = new ControlPoint();
		point.setControlPointId((long)0);
		point1.setControlPointId((long)1);
		point2.setControlPointId((long)2);
		point3.setControlPointId((long)3);
		point4.setControlPointId((long)4);
		point.setName("Parque das Rosas");
		point1.setName("Unimed");
		point2.setName("Igreja");
		point3.setName("Fashion Mall");
		point4.setName("Clube do flamengo");
		point.setPatternFraction((float)0);
		point1.setPatternFraction((float)0.5);
		point2.setPatternFraction((float)0.6);
		point3.setPatternFraction((float)0.8);
		point4.setPatternFraction((float)1);
		point.setPatternType(PatternType.OUTGOING);
		point1.setPatternType(PatternType.OUTGOING);
		point2.setPatternType(PatternType.OUTGOING);
		point3.setPatternType(PatternType.OUTGOING);
		point4.setPatternType(PatternType.OUTGOING);
		
		ControlPoint point5 = new ControlPoint();
		ControlPoint point6 = new ControlPoint();
		ControlPoint point7 = new ControlPoint();
		ControlPoint point8 = new ControlPoint();
		ControlPoint point9 = new ControlPoint();
		
		
		point5.setControlPointId((long)5);
		point6.setControlPointId((long)6);
		point7.setControlPointId((long)7);
		point8.setControlPointId((long)8);
		point9.setControlPointId((long)9);
		
		point5.setName("Clube do Flamengo");
		point6.setName("Fashion Mall");
		point7.setName("Igreja");
		point8.setName("Unimed");
		point9.setName("Parque das Rosas");
		
		point5.setPatternFraction((float)0);
		point6.setPatternFraction((float)0.5);
		point7.setPatternFraction((float)0.6);
		point8.setPatternFraction((float)0.8);
		point9.setPatternFraction((float)1);
		
		point5.setPatternType(PatternType.RETURNING);
		point6.setPatternType(PatternType.RETURNING);
		point7.setPatternType(PatternType.RETURNING);
		point8.setPatternType(PatternType.RETURNING);
		point9.setPatternType(PatternType.RETURNING);
		
		controlPoints = new ArrayList<ControlPoint>();
		controlPoints.add(point);
		controlPoints.add(point1);
		controlPoints.add(point2);
		controlPoints.add(point3);
		controlPoints.add(point4);
		controlPoints.add(point5);
		controlPoints.add(point6);
		controlPoints.add(point7);
		controlPoints.add(point8);
		controlPoints.add(point9);

	}

	private void generateMockVehicles() {
		Vehicle vehicle  = new Vehicle();
		Vehicle vehicle1 = new Vehicle();
		Vehicle vehicle2 = new Vehicle();
		Vehicle vehicle3 = new Vehicle();
		Vehicle vehicle4 = new Vehicle();
		vehicle.setVehicleCode("1234");
		vehicle1.setVehicleCode("234");
		vehicle2.setVehicleCode("123");
		vehicle3.setVehicleCode("134");
		vehicle4.setVehicleCode("1245");
		vehicle.setPatternFraction((double) 0.23);
		vehicle1.setPatternFraction((double) 0.76);
		vehicle2.setPatternFraction((double) 0.98);
		vehicle3.setPatternFraction((double) 0.10);
		vehicle4.setPatternFraction((double) 0.20);
		vehicle.setPatternType(PatternType.OUTGOING.ordinal());
		vehicle1.setPatternType(PatternType.OUTGOING.ordinal());
		vehicle2.setPatternType(PatternType.RETURNING.ordinal());
		vehicle3.setPatternType(PatternType.RETURNING.ordinal());
		vehicle2.setPatternType(PatternType.RETURNING.ordinal());
		vehicles = new ArrayList<Vehicle>();
		vehicles.add(vehicle);
		vehicles.add(vehicle1);
		vehicles.add(vehicle2);
		vehicles.add(vehicle3);
		vehicles.add(vehicle4);
	}



}
