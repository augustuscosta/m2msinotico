package br.com.m2msolutions.sinotico.cloud;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;




public class RestClient {

	static final String APP_IDENTIFIER = "user_tools";
	
	
	public enum RequestMethod
	{
	    GET,
	    POST
	}
	 
    private ArrayList <NameValuePair> params;
    private ArrayList <NameValuePair> headers;
 
    private String url;
 
    private int responseCode;
    private String message;
 
    private String response;
    
    public void cleanParams(){
    	params = new ArrayList<NameValuePair>();
        headers = new ArrayList<NameValuePair>();
    }
 
    public String getResponse() {
        return response;
    }
 
    public String getErrorMessage() {
        return message;
    }
 
    public int getResponseCode() {
        return responseCode;
    }
    
    public String getExceptionConstForMessage(){
    	String toReturn = "";
    	if(message != null){
    		if(message.indexOf("%") > 0 && message.lastIndexOf("%") > 0){
    			toReturn = message.substring(message.indexOf("%") + 1, message.lastIndexOf("%"));
    		}else{
    			toReturn = message;
    		}
    	}
    	return toReturn;
    }
    
    public String addSlashIfNeeded(String url){
    	if(url.lastIndexOf("/") != url.length() -1){
    		return url + "/";
    	}
    	return url;
    }
 
    public void addParam(String name, String value)
    {
        params.add(new BasicNameValuePair(name, value));
    }
 
    public void addHeader(String name, String value)
    {
        headers.add(new BasicNameValuePair(name, value));
    }
 
    public void execute(RequestMethod method) throws Exception
    {
        switch(method) {
            case GET:
            {
                //add parameters
                String combinedParams = "";
                if(!params.isEmpty()){
                    for(NameValuePair p : params)
                    {
                        String paramString = URLEncoder.encode(p.getValue(),"UTF-8");
                        combinedParams  += "/" + paramString;
                    }
                }
                
                String urlWithParams = url + combinedParams;
 
                HttpGet request = new HttpGet(urlWithParams);
 
                //add headers
                for(NameValuePair h : headers)
                {
                    request.addHeader(h.getName(), h.getValue());
                }
 
                executeRequest(request, urlWithParams);
                break;
            }
            case POST:
            {
                HttpPost request = new HttpPost(url);
 
                //add headers
                for(NameValuePair h : headers)
                {
                    request.addHeader(h.getName(), h.getValue());
                }
 
                if(!params.isEmpty()){
                    request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                }
 
                executeRequest(request, url);
                break;
            }
        }
    }
    
    public JSONArray getJsonObjectArrayFromResponse(String rootObjectName) throws JSONException{
    	if(response == null){
    		return null;
    	}
    	JSONObject jObject = new JSONObject(response);
    	return jObject.getJSONArray(rootObjectName);
    }
 
    private void executeRequest(HttpUriRequest request, String url)
    {
        HttpClient client = new DefaultHttpClient();
 
        HttpResponse httpResponse;
        InputStream instream;
        
        try {
            httpResponse = client.execute(request);
            responseCode = httpResponse.getStatusLine().getStatusCode();
            message = httpResponse.getStatusLine().getReasonPhrase();
 
            HttpEntity entity = httpResponse.getEntity();
 
            if (entity != null) {
 
                instream = entity.getContent();
                response = convertStreamToString(instream);
                instream.close();
            }
 
        } catch (ClientProtocolException e)  {
            client.getConnectionManager().shutdown();
            message = e.getLocalizedMessage();
        } catch (IOException e) {
            client.getConnectionManager().shutdown();
            message = e.getLocalizedMessage();
        }
    }
    
 
    private static String convertStreamToString(InputStream is) {
 
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
 
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

	public ArrayList<NameValuePair> getParams() {
		return params;
	}

	public void setParams(ArrayList<NameValuePair> params) {
		this.params = params;
	}

	public ArrayList<NameValuePair> getHeaders() {
		return headers;
	}

	public void setHeaders(ArrayList<NameValuePair> headers) {
		this.headers = headers;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public void setResponse(String response) {
		this.response = response;
	}
    
    
}
