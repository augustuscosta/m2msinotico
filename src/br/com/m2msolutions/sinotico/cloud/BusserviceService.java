package br.com.m2msolutions.sinotico.cloud;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import br.com.m2msolutions.sinotico.model.BusService;
import br.com.m2msolutions.sinotico.util.Session;

public class BusserviceService extends RestClient {
	
	public static final String BUS_SERVICE_LIST_PARAM_KEY = "BUS_SERVICE_LIST_PARAM_KEY";

	
	private final String GET_BUS_SERVICES_BY_LOCATION_URL = "ws/rest/mobile/getAllBusServices";
	
	private final String BUS_SERVICE_ROOT_OBJECT = "busServiceDTO";

	
	public void getAllBusServices(Context context) throws Exception{
		cleanParams();
		setUrl(addSlashIfNeeded(Session.getServer(context)) + GET_BUS_SERVICES_BY_LOCATION_URL);
		addParam("token", "no-token");
		execute(RequestMethod.GET);
	}
	
	public List<BusService> getBusServiceListFromResponse() throws JSONException{
		JSONArray jsonArray = getJsonObjectArrayFromResponse(BUS_SERVICE_ROOT_OBJECT);
		if(jsonArray != null){
			List<BusService> toReturn = new ArrayList<BusService>();
			for (int i = 0; i < jsonArray.length(); i++) {
				BusService busService = new BusService();
				if(jsonArray.getJSONObject(i).has("busServiceId")){
					busService.setBusServiceId(jsonArray.getJSONObject(i).getLong("busServiceId"));
				}
				if(jsonArray.getJSONObject(i).has("description")){
					busService.setDescription(jsonArray.getJSONObject(i).getString("description").toString());
				}
				if(jsonArray.getJSONObject(i).has("name")){
					busService.setName(jsonArray.getJSONObject(i).getString("name").toString());
				}
				if(jsonArray.getJSONObject(i).has("shortName")){
					busService.setShortName(jsonArray.getJSONObject(i).getString("shortName").toString());
				}
				if(jsonArray.getJSONObject(i).has("controlPointName")){
					busService.setControlPointName(jsonArray.getJSONObject(i).getString("controlPointName").toString());
				}
				if(jsonArray.getJSONObject(i).has("controlPointCode")){
					busService.setControlPointCode(jsonArray.getJSONObject(i).getLong("controlPointCode"));
				}
				if(jsonArray.getJSONObject(i).has("destinationName")){
					busService.setDestinationName(jsonArray.getJSONObject(i).getString("destinationName").toString());
				}
				if(jsonArray.getJSONObject(i).has("destinationId")){
					busService.setDestinationId(jsonArray.getJSONObject(i).getLong("destinationId"));
				}
				if(jsonArray.getJSONObject(i).has("codeNextVehicle")){
					busService.setCodeNextVehicle(jsonArray.getJSONObject(i).getString("codeNextVehicle").toString());
				}
				if(jsonArray.getJSONObject(i).has("arrivalNextTime")){
					busService.setArrivalNextTime(Integer.parseInt(jsonArray.getJSONObject(i).getString("arrivalNextTime").toString()));
				}
				if(jsonArray.getJSONObject(i).has("returnDestinationName")){
					busService.setReturnDestinationName(jsonArray.getJSONObject(i).getString("returnDestinationName").toString());
				}
				if(jsonArray.getJSONObject(i).has("returnDestinationId")){
					busService.setReturnDestinationId(jsonArray.getJSONObject(i).getLong("returnDestinationId"));
				}
				if(jsonArray.getJSONObject(i).has("returnCodeNextVehicle")){
					busService.setReturnCodeNextVehicle(jsonArray.getJSONObject(i).getString("returnCodeNextVehicle").toString());
				}
				if(jsonArray.getJSONObject(i).has("returnArrivalNextTime")){
					busService.setReturnArrivalNextTime(Integer.parseInt(jsonArray.getJSONObject(i).getString("returnArrivalNextTime").toString()));
				}
				if(jsonArray.getJSONObject(i).has("outgoingLength")){
					busService.setOutgoingLength(jsonArray.getJSONObject(i).getDouble("outgoingLength"));
				}
				
				if(jsonArray.getJSONObject(i).has("returningLength")){
					busService.setReturningLength(jsonArray.getJSONObject(i).getDouble("returningLength"));
				}
				
				if(jsonArray.getJSONObject(i).has("length")){
					busService.setLength(jsonArray.getJSONObject(i).getDouble("length"));
				}
				toReturn.add(busService);
			}
			return toReturn;
		}
		return null;
	}
	
}
