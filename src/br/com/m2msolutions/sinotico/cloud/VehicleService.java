package br.com.m2msolutions.sinotico.cloud;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import br.com.m2msolutions.sinotico.model.Vehicle;
import br.com.m2msolutions.sinotico.util.Session;

public class VehicleService extends RestClient {

	public static final String BUS_SERVICE_ID_PARAM_KEY = "BUS_SERVICE_ID";
	public static final String BUS_SERVICE_PARAM_KEY = "BUS_SERVICE";
	
	private final String GET_VEHICLES_BY_BUS_SERVICE = "ws/rest/mobile/getVehiclesByBusService";
	private final String VEHICLE_ROOT_OBJECT = "vehicleDTO";
	
	public void getVehiclesByBusService(Long busServiceId, Context context) throws Exception{
		cleanParams();
		setUrl(addSlashIfNeeded(Session.getServer(context)) + GET_VEHICLES_BY_BUS_SERVICE);
		addParam("busServiceId", busServiceId.toString());
		addParam("token", "no-token");
		execute(RequestMethod.GET);
	}
	
	public List<Vehicle> getVehicleListFromResponse() throws JSONException, ParseException{
		JSONArray jsonArray = getJsonObjectArrayFromResponse(VEHICLE_ROOT_OBJECT);
		if(jsonArray != null){
			List<Vehicle> toReturn = new ArrayList<Vehicle>();
			for (int i = 0; i < jsonArray.length(); i++) {
				Vehicle vehicle = new Vehicle();
				if(jsonArray.getJSONObject(i).has("id")){
					vehicle.setId(jsonArray.getJSONObject(i).getLong("id"));
				}
				if(jsonArray.getJSONObject(i).has("counter1")){
					vehicle.setCounter1(jsonArray.getJSONObject(i).getLong("counter1"));
				}
				if(jsonArray.getJSONObject(i).has("counter2")){
					vehicle.setCounter2(jsonArray.getJSONObject(i).getLong("counter2"));
				}
				if(jsonArray.getJSONObject(i).has("counter3")){
					vehicle.setCounter3(jsonArray.getJSONObject(i).getLong("counter3"));
				}
				if(jsonArray.getJSONObject(i).has("type1")){
					vehicle.setType1(jsonArray.getJSONObject(i).getInt("type1"));
				}
				if(jsonArray.getJSONObject(i).has("type2")){
					vehicle.setType2(jsonArray.getJSONObject(i).getInt("type2"));
				}
				if(jsonArray.getJSONObject(i).has("type3")){
					vehicle.setType3(jsonArray.getJSONObject(i).getInt("type3"));
				}
				if(jsonArray.getJSONObject(i).has("licensePlate")){
					vehicle.setLicensePlate(jsonArray.getJSONObject(i).getString("licensePlate").toString());
				}
				if(jsonArray.getJSONObject(i).has("vehicleCode")){
					vehicle.setVehicleCode(jsonArray.getJSONObject(i).getString("vehicleCode").toString());
				}
				if(jsonArray.getJSONObject(i).has("year")){
					vehicle.setYear(jsonArray.getJSONObject(i).getInt("year"));
				}
				if(jsonArray.getJSONObject(i).has("doorOpen")){
					vehicle.setDoorOpen(jsonArray.getJSONObject(i).getBoolean("doorOpen"));
				}
				if(jsonArray.getJSONObject(i).has("direction")){
					vehicle.setDirection((float)jsonArray.getJSONObject(i).getDouble("direction"));
				}
				if(jsonArray.getJSONObject(i).has("voltage")){
					vehicle.setVoltage((float)jsonArray.getJSONObject(i).getDouble("voltage"));
				}
				if(jsonArray.getJSONObject(i).has("internalAlert")){
					vehicle.setInternalAlert(jsonArray.getJSONObject(i).getBoolean("internalAlert"));
				}
				if(jsonArray.getJSONObject(i).has("panicAlert")){
					vehicle.setPanicAlert(jsonArray.getJSONObject(i).getBoolean("panicAlert"));
				}
				if(jsonArray.getJSONObject(i).has("speed")){
					vehicle.setSpeed((float)jsonArray.getJSONObject(i).getDouble("speed"));
				}
//				if(jsonArray.getJSONObject(i).has("timestamp")){
//					String timeStampString = jsonArray.getJSONObject(i).getString("timestamp");
//					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");  
//					Date date = (Date) formatter.parse(timeStampString);  
//					vehicle.setTimestamp(date);
//				}
				if(jsonArray.getJSONObject(i).has("odometer")){
					vehicle.setOdometer((float)jsonArray.getJSONObject(i).getDouble("odometer"));
				}
				if(jsonArray.getJSONObject(i).has("lifeTimer")){
					vehicle.setLifeTimer(jsonArray.getJSONObject(i).getInt("lifeTimer"));
				}
				if(jsonArray.getJSONObject(i).has("ignitionTimeHour")){
					vehicle.setIgnitionTimeHour((float)jsonArray.getJSONObject(i).getDouble("ignitionTimeHour"));
				}
				if(jsonArray.getJSONObject(i).has("paneAlert")){
					vehicle.setPaneAlert(jsonArray.getJSONObject(i).getBoolean("paneAlert"));
				}
				if(jsonArray.getJSONObject(i).has("ignition")){
					vehicle.setIgnition(jsonArray.getJSONObject(i).getBoolean("ignition"));
				}
				if(jsonArray.getJSONObject(i).has("latitude")){
					vehicle.setLatitude((float)jsonArray.getJSONObject(i).getDouble("latitude"));
				}
				if(jsonArray.getJSONObject(i).has("longitude")){
					vehicle.setLongitude((float)jsonArray.getJSONObject(i).getDouble("longitude"));
				}
				if(jsonArray.getJSONObject(i).has("temperature")){
					vehicle.setTemperature((float)jsonArray.getJSONObject(i).getDouble("temperature"));
				}
				if(jsonArray.getJSONObject(i).has("calculatedSpeed")){
					vehicle.setCalculatedSpeed(jsonArray.getJSONObject(i).getDouble("calculatedSpeed"));
				}
				if(jsonArray.getJSONObject(i).has("roletteCounter")){
					vehicle.setRoletteCounter(jsonArray.getJSONObject(i).getLong("roletteCounter"));
				}
				if(jsonArray.getJSONObject(i).has("gprsOnline")){
					vehicle.setGprsOnline(jsonArray.getJSONObject(i).getBoolean("gprsOnline"));
				}
				if(jsonArray.getJSONObject(i).has("cam")){
					JSONArray array = jsonArray.getJSONObject(i).getJSONObject("cam").getJSONArray("url");
					vehicle.setUrls(new ArrayList<String>());
					for(int z = 0; z < array.length(); z++){
						vehicle.getUrls().add(array.get(z).toString());
					}
				}
				if(jsonArray.getJSONObject(i).has("patternType")){
					vehicle.setPatternType(jsonArray.getJSONObject(i).getInt("patternType"));
				}
				if(jsonArray.getJSONObject(i).has("patternFraction")){
					vehicle.setPatternFraction(jsonArray.getJSONObject(i).getDouble("patternFraction"));
				}
				if(jsonArray.getJSONObject(i).has("controlPointId")){
					vehicle.setControlPointId(jsonArray.getJSONObject(i).getLong("controlPointId"));
				}
				toReturn.add(vehicle);
			}
			return toReturn;
		}
		return null;
	}
}
