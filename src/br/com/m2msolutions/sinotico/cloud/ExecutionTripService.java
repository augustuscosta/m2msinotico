package br.com.m2msolutions.sinotico.cloud;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import br.com.m2msolutions.sinotico.model.ControlPoint;
import br.com.m2msolutions.sinotico.model.ExecutionPoint;
import br.com.m2msolutions.sinotico.util.Session;

public class ExecutionTripService extends RestClient {

	private final String GET_EXECUTION_TRIP_URL = "ws/rest/mobile/getExecutionTripValidJourney";
	private final String EXECUTION_TRIP_ROOT_OBJECT = "executionPointDTO";
	
	public void getExecutionTripValidJourney(Context context, ControlPoint controlPoint) throws Exception{
		cleanParams();
		setUrl(addSlashIfNeeded(Session.getServer(context)) + GET_EXECUTION_TRIP_URL);
		addParam("controlPointId", controlPoint.toString());
		addParam("token", "no-token");
		execute(RequestMethod.GET);
	}
	
	public List<ExecutionPoint> getForecastListFromResponse() throws JSONException, ParseException{
		
		JSONArray jsonArray = getJsonObjectArrayFromResponse(EXECUTION_TRIP_ROOT_OBJECT);
		if(jsonArray != null){
			List<ExecutionPoint> toReturn = new ArrayList<ExecutionPoint>();
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
			ExecutionPoint point;
			for (int i = 0; i < jsonArray.length(); i++) {
				point = new ExecutionPoint();
				
				if(jsonArray.getJSONObject(i).has("busServiceId")){
					point.setBusServiceId(jsonArray.getJSONObject(i).getLong("busServiceId"));
				}
				if(jsonArray.getJSONObject(i).has("busServiceName")){
					point.setBusServiceName(jsonArray.getJSONObject(i).getString("busServiceName").toString());
				}
				if(jsonArray.getJSONObject(i).has("TripProgramId")){
					point.setTripProgramId(jsonArray.getJSONObject(i).getLong("TripProgramId"));
				}
				if(jsonArray.getJSONObject(i).has("busTable")){
					point.setBusTable(jsonArray.getJSONObject(i).getInt("busTable"));
				}
				if(jsonArray.getJSONObject(i).has("programVehicleCode")){
					point.setProgramVehicleCode(jsonArray.getJSONObject(i).getString("programVehicleCode").toString());
				}
				if(jsonArray.getJSONObject(i).has("programKm")){
					point.setProgramKm(Double.parseDouble(jsonArray.getJSONObject(i).getString("programKm").toString()));
				}
				if(jsonArray.getJSONObject(i).has("programDeparture")){
					String timeStampString = jsonArray.getJSONObject(i).getString("programDeparture");
					Date date = (Date) formatter.parse(timeStampString);  
					point.setProgramDeparture(date);
				}
				if(jsonArray.getJSONObject(i).has("departure")){
					String timeStampString = jsonArray.getJSONObject(i).getString("departure");
					Date date = (Date) formatter.parse(timeStampString);  
					point.setDeparture(date);
				}
				if(jsonArray.getJSONObject(i).has("departureDiference")){
					point.setDepartureDiference(jsonArray.getJSONObject(i).getInt("departureDiference"));
				}
				if(jsonArray.getJSONObject(i).has("departureInterval")){
					point.setDepartureInterval(jsonArray.getJSONObject(i).getInt("departureInterval"));
				}
				if(jsonArray.getJSONObject(i).has("programDepartureInterval")){
					point.setProgramDepartureInterval(jsonArray.getJSONObject(i).getInt("programDepartureInterval"));
				}
				if(jsonArray.getJSONObject(i).has("programReturn")){
					String timeStampString = jsonArray.getJSONObject(i).getString("programReturn");
					Date date = (Date) formatter.parse(timeStampString);  
					point.setProgramReturn(date);
				}
				if(jsonArray.getJSONObject(i).has("realReturn")){
					String timeStampString = jsonArray.getJSONObject(i).getString("realReturn");
					Date date = (Date) formatter.parse(timeStampString);  
					point.setRealReturn(date);
				}
				if(jsonArray.getJSONObject(i).has("returnDiference")){
					point.setReturnDiference(jsonArray.getJSONObject(i).getInt("returnDiference"));
				}
				if(jsonArray.getJSONObject(i).has("returnInterval")){
					point.setReturnInterval(jsonArray.getJSONObject(i).getInt("returnInterval"));
				}
				if(jsonArray.getJSONObject(i).has("programReturnInterval")){
					point.setProgramReturnInterval(jsonArray.getJSONObject(i).getInt("programReturnInterval"));
				}
				if(jsonArray.getJSONObject(i).has("executedKm")){
					point.setExecutedKm(Double.parseDouble(jsonArray.getJSONObject(i).getString("executedKm").toString()));
				}
				if(jsonArray.getJSONObject(i).has("pattern")){
					point.setPattern(jsonArray.getJSONObject(i).getString("pattern").toString());
				}
				toReturn.add(point);
				System.gc();
			}
			return toReturn;
		}
		return null;
	}
}
