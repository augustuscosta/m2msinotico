package br.com.m2msolutions.sinotico.cloud;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import br.com.m2msolutions.sinotico.model.ControlPoint;
import br.com.m2msolutions.sinotico.model.PatternType;
import br.com.m2msolutions.sinotico.util.Session;

public class ControlPointService extends RestClient {

	private final String GET_CONTROLPOINTS_BY_BUS_SERVICE = "ws/rest/mobile/getControlPointsByBusService";
	private final String GET_CONTROLPOINTS_BY_LOCATION = "ws/rest/mobile/getControlPointsByLocation";
	private final String CONTROLPOINT_ROOT_OBJECT = "controlPointDTO";
	
	public void getControlPointsByBusService(Long busServiceId,Context context) throws Exception{
		cleanParams();
		setUrl(addSlashIfNeeded(Session.getServer(context)) + GET_CONTROLPOINTS_BY_BUS_SERVICE);
		addParam("busServiceId", busServiceId.toString());
		addParam("token", "no-token");
		execute(RequestMethod.GET);
	}
	
	public void getControlPointsByLocation(Double latitude, Double longitude, Integer raio ,Context context) throws Exception{
		cleanParams();
		setUrl(addSlashIfNeeded(Session.getServer(context)) + GET_CONTROLPOINTS_BY_LOCATION);
		addParam("latitude", latitude.toString());
		addParam("longitude", longitude.toString());
		addParam("meterDistance", raio.toString());
		addParam("token", "no-token");
		execute(RequestMethod.GET);
	}
	
	public List<ControlPoint> getControlPointListFromResponse() throws JSONException, ParseException{
		JSONArray jsonArray = getJsonObjectArrayFromResponse(CONTROLPOINT_ROOT_OBJECT);
		if(jsonArray != null){
			List<ControlPoint> toReturn = new ArrayList<ControlPoint>();
			for (int i = 0; i < jsonArray.length(); i++) {
				ControlPoint controlPoint = new ControlPoint();
				if(jsonArray.getJSONObject(i).has("controlPointId")){
					controlPoint.setControlPointId(jsonArray.getJSONObject(i).getLong("controlPointId"));
				}
				if(jsonArray.getJSONObject(i).has("pointLocationId")){
					controlPoint.setPointLocationId(jsonArray.getJSONObject(i).getLong("pointLocationId"));
				}
				if(jsonArray.getJSONObject(i).has("name")){
					controlPoint.setName(jsonArray.getJSONObject(i).getString("name").toString());
				}
				if(jsonArray.getJSONObject(i).has("patternFraction")){
					controlPoint.setPatternFraction((float)jsonArray.getJSONObject(i).getDouble("patternFraction"));
				}
				if(jsonArray.getJSONObject(i).has("controlPointTypeInt")){
					controlPoint.setControlPointTypeInt(jsonArray.getJSONObject(i).getInt("controlPointTypeInt"));
				}
				if(jsonArray.getJSONObject(i).has("distanceIntoPattern")){
					controlPoint.setDistanceIntoPattern(jsonArray.getJSONObject(i).getDouble("distanceIntoPattern"));
				}
				if(jsonArray.getJSONObject(i).has("latitude")){
					controlPoint.setLatitude(jsonArray.getJSONObject(i).getDouble("latitude"));
				}
				if(jsonArray.getJSONObject(i).has("longitude")){
					controlPoint.setLongitude(jsonArray.getJSONObject(i).getDouble("longitude"));
				}
				if(jsonArray.getJSONObject(i).has("hasProgram")){
					controlPoint.setHasProgram(jsonArray.getJSONObject(i).getBoolean("hasProgram"));
				}
				if(jsonArray.getJSONObject(i).has("patternType")){
					controlPoint.setPatternType(PatternType.getType(jsonArray.getJSONObject(i).getInt("patternType")));
				}
				toReturn.add(controlPoint);
			}
			return toReturn;
		}
		return null;
	}
}
