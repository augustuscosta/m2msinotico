package br.com.m2msolutions.sinotico;

import roboguice.activity.RoboActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import br.com.m2msolutions.sinotico.util.LogUtil;

public class SplashScreenActivity extends RoboActivity {
	
	protected boolean spashRunning = true;
	protected int splashRunTime = 5000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		
		Thread splashScreenTread = new Thread() {
			@Override
			public void run() {
				try {
					int waited = 0;
					while (spashRunning && (waited < splashRunTime)) {
						sleep(100);
						if (spashRunning) {
							waited += 100;
						}
					}
				} catch (InterruptedException e) {
					LogUtil.e("erro ao fechar splashScreen", e);
					
				} finally {
					finish();
				}
			}
		};
		splashScreenTread.start();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			spashRunning = false;
		}
		return true;
	}

}
