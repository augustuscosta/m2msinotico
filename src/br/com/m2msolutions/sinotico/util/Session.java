package br.com.m2msolutions.sinotico.util;

import android.content.Context;
import android.content.SharedPreferences;

public class Session {
	
	public static final String PREFS 					= "M2M_USER_TOOLS_SESSION_PREFS";
	public static final String SERVER_PREFS_KEY 		= "M2M_USER_TOOLS_SERVER_PREFS_KEY";
	public static final String HELP_URL 				= "M2M_HELP_URL_PREFS_KEY";
	public static final String SELECT_LOCATION_HELP		= "M2M_SEARCH_LOCATION_HELP";
	public static final String RATING					= "M2M_RATING";
	public static final String SEARCH_CHOICE_HELP	    = "M2M_CHOICE_HELP";
	public static final String SEARCH_TABLE_HELP	    = "M2M_TABLE_HELP";
	public static final String SEARCH_SAVED_TABLE_HELP  = "M2M_SAVED_TABLE_HELP";
	public static final String BUS_SERVICE_HELP  		= "M2M_BUS_SERVICE_HELP";
	public static final String ENQUETE_HELP  			= "M2M_ENQUETE_HELP";
	public static final String TWITTER_HELP  			= "M2M_TWITTER_HELP";
	

	private static SharedPreferences settings;
	
	private static SharedPreferences getSharedPreferencesInstance(Context context){
		if(settings == null){
			settings = context.getSharedPreferences(PREFS, 0);
		}
		return settings;
	}
	
	public static void setServer(String server,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.putString(SERVER_PREFS_KEY, server);
		editor.commit();
	}
	
	public static String getServer(Context context){
		//return getSharedPreferencesInstance(context).getString(SERVER_PREFS_KEY, "http://192.168.171.182:8888");
		return getSharedPreferencesInstance(context).getString(SERVER_PREFS_KEY, "http://173.224.120.97:8850/m2mfrotaweb");
	}
	
	public static void setHelpUrl(String url,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.putString(HELP_URL, url);
		editor.commit();
	}
	
	public static String getSHelpUrl(Context context){
		return getSharedPreferencesInstance(context).getString(HELP_URL, "http://help.cochilo.m2mfrota.com");
	}
	
	public static void setLocationHelp(Boolean help,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.putBoolean(SELECT_LOCATION_HELP, help);
		editor.commit();
	}
	
	public static Boolean getLocationHelp(Context context){
		return getSharedPreferencesInstance(context).getBoolean(SELECT_LOCATION_HELP, false);
	}

	public static boolean getRating(Context context) {
		return getSharedPreferencesInstance(context).getBoolean(RATING, false);
	}
	
	public static void setRating(Boolean rating,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.putBoolean(RATING, rating);
		editor.commit();
	}

	public static boolean getSearchChoiceHelp(Context context) {
		return getSharedPreferencesInstance(context).getBoolean(SEARCH_CHOICE_HELP, false);
	}
	
	public static void setSearchChoiceHelp(Boolean rating,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.putBoolean(SEARCH_CHOICE_HELP, rating);
		editor.commit();
	}

	public static void setSearchTabHelp(boolean b, Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.putBoolean(SEARCH_TABLE_HELP, b);
		editor.commit();
	}

	public static boolean getSearchTabHelp(Context context) {
		return getSharedPreferencesInstance(context).getBoolean(SEARCH_TABLE_HELP, false);
	}

	public static boolean getSearchSavedTabHelp(Context context) {
		return getSharedPreferencesInstance(context).getBoolean(SEARCH_SAVED_TABLE_HELP, false);
	}

	public static void setSearchSavedTabHelp(boolean b,
			Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.putBoolean(SEARCH_SAVED_TABLE_HELP, b);
		editor.commit();
		
	}
	
	public static boolean getBusServiceHelp(Context context) {
		return getSharedPreferencesInstance(context).getBoolean(BUS_SERVICE_HELP, false);
	}

	public static void setBusServiceHelp(boolean b,
			Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.putBoolean(BUS_SERVICE_HELP, b);
		editor.commit();
		
	}
	
	public static boolean getSurveyHelp(Context context) {
		return getSharedPreferencesInstance(context).getBoolean(ENQUETE_HELP, false);
	}

	public static void setSurveyHelp(boolean b,
			Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.putBoolean(ENQUETE_HELP, b);
		editor.commit();
		
	}
	
	public static boolean getTwitterHelp(Context context) {
		return getSharedPreferencesInstance(context).getBoolean(TWITTER_HELP, false);
	}

	public static void setTwitterHelp(boolean b,
			Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.putBoolean(TWITTER_HELP, b);
		editor.commit();
		
	}
	
}
