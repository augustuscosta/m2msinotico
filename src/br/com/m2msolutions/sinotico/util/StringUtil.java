package br.com.m2msolutions.sinotico.util;

/**
 * 
 * @author pierrediderot@gmail.com
 * @since 27/02/2012
 * @version $Revision:  $ <br>
 *          $Date:  $ <br> 
 *          $Author:  $
 */
public class StringUtil {

	/**
	 * @param value
	 * @return True if value is not null and trim().length() > 0, otherwise false.
	 */
	public static boolean isValid(final String value) {
		return value != null && value.trim().length() > 0;
	}
	
	public static String trimString(String value){
		
		
		return value;
	}
	
}
