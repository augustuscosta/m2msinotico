package br.com.m2msolutions.sinotico.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.m2msolutions.sinotico.R;

public class LineCustomAdapter extends BaseAdapter {

	private Activity context;
	private List<String> lines;
	private List<String> linesPositions;
	
	public LineCustomAdapter(Activity context,List<String> lines,List<String> linesPositions) {
		this.context = context;
		this.lines = lines;
		this.linesPositions = linesPositions;
	}

	@Override
	public int getCount() {
		return lines == null ?0 : lines.size();
	}

	@Override
	public String getItem(int position) {
		return lines == null ? null: lines.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		String lineName = lines.get(position);
		String linePosition = linesPositions.get(position);
		LineCustomRowHolderInfo rowInfo;
		View row;
		if(convertView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.line_custom_row, null);
			rowInfo = new LineCustomRowHolderInfo();
			rowInfo.lineName = (TextView) row.findViewById(R.id.rowName);
			rowInfo.linePosition = (TextView) row.findViewById(R.id.rowPosition);
		}else{
			row = convertView;
			rowInfo = (LineCustomRowHolderInfo) row.getTag();
		}
		rowInfo.drawRow(lineName, linePosition);
		row.setTag(rowInfo);
		
		return row;
	}

}
