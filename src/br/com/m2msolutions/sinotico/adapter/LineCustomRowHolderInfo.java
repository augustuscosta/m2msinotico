package br.com.m2msolutions.sinotico.adapter;

import android.widget.TextView;

public class LineCustomRowHolderInfo {
	
	public TextView lineName;
	public TextView linePosition;
	
	public void drawRow(String name, String position){
		if(name == null){
			lineName.setText("");
		}else{
			lineName.setText(name);
		}
		
		if(position == null ){
			linePosition.setText("");
		}else{
			linePosition.setText(position);
		}
		
	}


}
