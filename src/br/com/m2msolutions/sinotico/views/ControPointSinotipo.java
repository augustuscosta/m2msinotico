package br.com.m2msolutions.sinotico.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.view.View;
import br.com.m2msolutions.sinotico.R;
import br.com.m2msolutions.sinotico.model.ControlPoint;

@SuppressLint({ "DrawAllocation", "ViewConstructor" })
public class ControPointSinotipo extends View /*implements View.OnTouchListener*/{

	private int x;
	private int y;
	private Bitmap image;
	private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private ControlPoint controlPoint;
	private Context context;
	
	public ControPointSinotipo(Context context, int x, int y, ControlPoint controlPoint) {
		super(context);
		this.context = context;
		this.x = x;
		this.y = y;
		this.controlPoint = controlPoint;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
	    super.onDraw(canvas);
	    image = BitmapFactory.decodeResource(getResources(), R.drawable.bustop);
	    canvas.drawBitmap(image,x - image.getWidth()/2 ,y + image.getHeight()/4 ,mPaint);
	    
	    
	    mPaint.setColor(Color.WHITE); 
	    mPaint.setStyle(Style.FILL); 
	    mPaint.setColor(Color.BLACK); 
	    mPaint.setTextSize(25); 
	    mPaint.setFakeBoldText(true);
	    canvas.drawText(getLabelToDraw(controlPoint.getName()), x + 30, y +40, mPaint);
	}

	private String getLabelToDraw(String name) {
			if(name.length() >= 35){
				return name.substring(0, 15)+context.getString(R.string.triple_dots)+name.substring(name.length()-16, name.length());
			}
			return name;
	}

	public int getViewX() {
		return x;
	}
	public void setViewX(int x) {
		this.x = x;
	}
	public float getViewY() {
		return y;
	}
	public void setViewY(int y) {
		this.y = y;
	}

/*  private void showInfo(){
		Activity context = (Activity) getContext();
		DecimalFormat df = new DecimalFormat(); 
		LayoutInflater inflater = context.getLayoutInflater();
		View layout = inflater.inflate(R.layout.control_point_toast,(ViewGroup) findViewById(R.id.toast_layout_root));

		TextView text = (TextView) layout.findViewById(R.id.controlPointName);
		text.setText(controlPoint.getName());
		
		if(controlPoint.getPatternFraction() != null){
			text = (TextView) layout.findViewById(R.id.patternFraction);
    	    df.applyPattern("00.0;(00.0)");
    	    text.setText(df.format(controlPoint.getPatternFraction() * 100) + " %");
    	}

		Toast toast = new Toast(context);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(layout);
		toast.show();
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_DOWN){
//			if (checkIfWasTouched(event.getX(), event.getY())) {
				showInfo();
//			}
		}
		return false;
	}

	private boolean checkIfWasTouched(float x, float y) {
		if(x >= this.x && x <= (this.x + 32)){
			if(y >= this.y && y <= (this.y + 32)){
				return true;
			}
		}
		return false;
	}
*/	
}

