package br.com.m2msolutions.sinotico.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

@SuppressLint("ViewConstructor")
public class LineSinotipo extends View {

	private int x;
	private int y;
	private int lineWidth;
	private int lineHeight;
	private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private int color = 0xFF528cb8;
	
	public LineSinotipo(Context context, int x, int y, int lineWidth,int lineHeight) {
		super(context);
		this.x = x;
		this.y = y;
		this.lineWidth = lineWidth;
		this.lineHeight = lineHeight;
		this.mPaint.setStrokeWidth(40);
		mPaint.setAlpha(30);
		mPaint.setColor(color);
	}
	@Override
	protected void onDraw(Canvas canvas) {
	    super.onDraw(canvas);
	    canvas.drawLine(x, y, lineWidth, lineHeight, mPaint);
	}
	
	public int getViewX() {
		return x;
	}
	public void setViewX(int x) {
		this.x = x;
	}
	public int getViewY() {
		return y;
	}
	public void setViewY(int y) {
		this.y = y;
	}
	public int getLineWidth() {
		return lineWidth;
	}
	public void setLineWidth(int lineWidth) {
		this.lineWidth = lineWidth;
	}
	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}
	
	
}
