package br.com.m2msolutions.sinotico.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import br.com.m2msolutions.sinotico.R;
import br.com.m2msolutions.sinotico.model.PatternType;

public class PatternSinotipo extends View {

	private PatternType patternType;
	
	private int x;
	private int y;
	private Bitmap image;
	private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	
	public PatternSinotipo(Context context, int x, int y, PatternType patternType) {
		super(context);
		this.patternType = patternType;
		this.y = y;
		this.x = x;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
	    super.onDraw(canvas);
	    if(patternType.equals(PatternType.OUTGOING)){
	    	image = BitmapFactory.decodeResource(getResources(), R.drawable.outgoing);
	    }else if(patternType.equals(PatternType.RETURNING)){
	    	image = BitmapFactory.decodeResource(getResources(), R.drawable.returning);
	    }
	    canvas.drawBitmap(image,x,y,mPaint);
	}

	public int getViewX() {
		return x;
	}
	public void setViewX(int x) {
		this.x = x;
	}
	public int getViewY() {
		return y;
	}
	public void setViewY(int y) {
		this.y = y;
	}

}
