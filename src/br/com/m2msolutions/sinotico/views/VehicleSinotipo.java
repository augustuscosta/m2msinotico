
package br.com.m2msolutions.sinotico.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.MotionEvent;
import android.view.View;
import br.com.m2msolutions.sinotico.R;
import br.com.m2msolutions.sinotico.model.Vehicle;

@SuppressLint({ "DrawAllocation", "ViewConstructor" })
public class VehicleSinotipo extends View implements View.OnTouchListener{

	
	private int x;
	private int y;
	private Bitmap image;
	private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private final Paint paint = new Paint();
	private Vehicle vehicle;
	
	public VehicleSinotipo(Context context, int x, int y, Vehicle vehicle) {
		super(context);
		this.vehicle = vehicle;
		this.x = x - x/2;
		this.y = y;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
	    super.onDraw(canvas);
	    image = BitmapFactory.decodeResource(getResources(), R.drawable.btnbus);
	    canvas.drawBitmap(image,x,y,mPaint);
	    
	    /*paint.setColor(Color.WHITE); 
	    paint.setStyle(Style.FILL); 
	    paint.setTextSize(30); 
	    canvas.drawText(vehicle.getVehicleCode(), x + 80, y +40, paint);*/
	}
	
	public int getViewX() {
		return x;
	}
	public void setViewX(int x) {
		this.x = x;
	}
	public int getViewY() {
		return y;
	}
	public void setViewY(int y) {
		this.y = y;
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return false;
	}

}
