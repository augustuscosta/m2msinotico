package br.com.m2msolutions.sinotico.views;


public interface ControlPointSelector {
	
	void pointSelected(int position);

}
