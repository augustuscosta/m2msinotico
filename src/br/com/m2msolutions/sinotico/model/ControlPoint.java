package br.com.m2msolutions.sinotico.model;


public class ControlPoint {

	public enum ControlPointType
    {
        STOP,
        CONTROL,
        INITIAL,
        FINAL,
        FISCAL,
    }
	
	private Long controlPointId;
	
	private Long pointLocationId;
	
	private String name;
	
	private Float patternFraction;
	
	private Integer controlPointTypeInt;
	
	private Double distanceIntoPattern;
	
	private Double latitude;
	
	private Double longitude;
	
	private boolean hasProgram;
	
	private PatternType patternType;

	public Long getControlPointId() {
		return controlPointId;
	}

	public void setControlPointId(Long controlPointId) {
		this.controlPointId = controlPointId;
	}

	public Long getPointLocationId() {
		return pointLocationId;
	}

	public void setPointLocationId(Long pointLocationId) {
		this.pointLocationId = pointLocationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getPatternFraction() {
		return patternFraction;
	}

	public void setPatternFraction(Float patternFraction) {
		this.patternFraction = patternFraction;
	}

	public Integer getControlPointTypeInt() {
		return controlPointTypeInt;
	}

	public void setControlPointTypeInt(Integer controlPointTypeInt) {
		this.controlPointTypeInt = controlPointTypeInt;
	}

	public Double getDistanceIntoPattern() {
		return distanceIntoPattern;
	}

	public void setDistanceIntoPattern(Double distanceIntoPattern) {
		this.distanceIntoPattern = distanceIntoPattern;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public boolean isHasProgram() {
		return hasProgram;
	}

	public void setHasProgram(boolean hasProgram) {
		this.hasProgram = hasProgram;
	}

	public PatternType getPatternType() {
		return patternType;
	}

	public void setPatternType(PatternType patternType) {
		this.patternType = patternType;
	}

	
}
