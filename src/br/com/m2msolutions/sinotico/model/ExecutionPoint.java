package br.com.m2msolutions.sinotico.model;

import java.util.Date;

public class ExecutionPoint {

	private Long busServiceId;
	private String busServiceName;
	private Long TripProgramId;
	private Integer busTable;
	private String programVehicleCode;
	private Double programKm;
	private Date programDeparture;
	private Date departure;
	private Integer departureDiference;
	private Integer departureInterval;
	private Integer programDepartureInterval;
	private Date programReturn;
	private Date realReturn;
	private Integer returnDiference;
	private Integer returnInterval;
	private Integer programReturnInterval;
	private Double executedKm;
	private String pattern;
	
	public Long getBusServiceId() {
		return busServiceId;
	}
	public void setBusServiceId(Long busServiceId) {
		this.busServiceId = busServiceId;
	}
	public String getBusServiceName() {
		return busServiceName;
	}
	public void setBusServiceName(String busServiceName) {
		this.busServiceName = busServiceName;
	}
	public Long getTripProgramId() {
		return TripProgramId;
	}
	public void setTripProgramId(Long tripProgramId) {
		TripProgramId = tripProgramId;
	}
	public Integer getBusTable() {
		return busTable;
	}
	public void setBusTable(Integer busTable) {
		this.busTable = busTable;
	}
	public String getProgramVehicleCode() {
		return programVehicleCode;
	}
	public void setProgramVehicleCode(String programVehicleCode) {
		this.programVehicleCode = programVehicleCode;
	}
	public Double getProgramKm() {
		return programKm;
	}
	public void setProgramKm(Double programKm) {
		this.programKm = programKm;
	}
	public Date getProgramDeparture() {
		return programDeparture;
	}
	public void setProgramDeparture(Date programDeparture) {
		this.programDeparture = programDeparture;
	}
	public Date getDeparture() {
		return departure;
	}
	public void setDeparture(Date departure) {
		this.departure = departure;
	}
	public Integer getDepartureDiference() {
		return departureDiference;
	}
	public void setDepartureDiference(Integer departureDiference) {
		this.departureDiference = departureDiference;
	}
	public Integer getDepartureInterval() {
		return departureInterval;
	}
	public void setDepartureInterval(Integer departureInterval) {
		this.departureInterval = departureInterval;
	}
	public Integer getProgramDepartureInterval() {
		return programDepartureInterval;
	}
	public void setProgramDepartureInterval(Integer programDepartureInterval) {
		this.programDepartureInterval = programDepartureInterval;
	}
	public Date getProgramReturn() {
		return programReturn;
	}
	public void setProgramReturn(Date programReturn) {
		this.programReturn = programReturn;
	}
	public Date getRealReturn() {
		return realReturn;
	}
	public void setRealReturn(Date realReturn) {
		this.realReturn = realReturn;
	}
	public Integer getReturnDiference() {
		return returnDiference;
	}
	public void setReturnDiference(Integer returnDiference) {
		this.returnDiference = returnDiference;
	}
	public Integer getReturnInterval() {
		return returnInterval;
	}
	public void setReturnInterval(Integer returnInterval) {
		this.returnInterval = returnInterval;
	}
	public Integer getProgramReturnInterval() {
		return programReturnInterval;
	}
	public void setProgramReturnInterval(Integer programReturnInterval) {
		this.programReturnInterval = programReturnInterval;
	}
	public Double getExecutedKm() {
		return executedKm;
	}
	public void setExecutedKm(Double executedKm) {
		this.executedKm = executedKm;
	}
	public String getPattern() {
		return pattern;
	}
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	
	
}
