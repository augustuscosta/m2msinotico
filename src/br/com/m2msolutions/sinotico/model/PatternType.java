package br.com.m2msolutions.sinotico.model;

public enum PatternType {

	UNDETERMINED,
	OUTGOING,
	RETURNING,
	CIRCULAR,
	OUTOFROUTE,
	TURNPOINT;
	
	public static PatternType getType(int type){
		if(PatternType.UNDETERMINED.ordinal() == type){
			return PatternType.UNDETERMINED;
		}
		if(PatternType.OUTGOING.ordinal() == type){
			return PatternType.OUTGOING;
		}
		if(PatternType.RETURNING.ordinal() == type){
			return PatternType.RETURNING;
		}
		if(PatternType.CIRCULAR.ordinal() == type){
			return PatternType.CIRCULAR;
		}
		if(PatternType.OUTOFROUTE.ordinal() == type){
			return PatternType.OUTOFROUTE;
		}
		return PatternType.TURNPOINT;
	}
}
