package br.com.m2msolutions.sinotico.model;

import java.io.Serializable;
import java.util.List;

public class Vehicle implements Serializable{

	private static final long serialVersionUID = 6619051940472147248L;

	public enum CounterType{OFF,SUBIDA,DESCIDA,RPM}
	
	private Long id;
	
	private Long counter1;

	private Long counter2;

	private Long counter3;

	private Integer type1;

	private Integer type2;

	private Integer type3;
	
	private String licensePlate;
	
	private String vehicleCode;
	
	private Integer year;
	
	private Boolean doorOpen;

	private Float direction;

	private Float voltage;

	private Boolean internalAlert;

	private Boolean panicAlert;

	private Float speed;

	private Float odometer;

	private Integer lifeTimer;
	
	private Float ignitionTimeHour;

	private Boolean paneAlert;
	
	private Boolean ignition;

	private Float latitude;

	private Float longitude;

	private Float temperature;

    private Double calculatedSpeed;
    
    private Long roletteCounter;
    
    private Boolean gprsOnline;
	
	private List<String> urls;
	
	private Integer patternType;
	
	private Double patternFraction;
	
	private Long controlPointId;
	
	

	public Long getControlPointId() {
		return controlPointId;
	}

	public void setControlPointId(Long controlPointId) {
		this.controlPointId = controlPointId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCounter1() {
		return counter1;
	}

	public void setCounter1(Long counter1) {
		this.counter1 = counter1;
	}

	public Long getCounter2() {
		return counter2;
	}

	public void setCounter2(Long counter2) {
		this.counter2 = counter2;
	}

	public Long getCounter3() {
		return counter3;
	}

	public void setCounter3(Long counter3) {
		this.counter3 = counter3;
	}

	public Integer getType1() {
		return type1;
	}

	public void setType1(Integer type1) {
		this.type1 = type1;
	}

	public Integer getType2() {
		return type2;
	}

	public void setType2(Integer type2) {
		this.type2 = type2;
	}

	public Integer getType3() {
		return type3;
	}

	public void setType3(Integer type3) {
		this.type3 = type3;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getVehicleCode() {
		return vehicleCode;
	}

	public void setVehicleCode(String vehicleCode) {
		this.vehicleCode = vehicleCode;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Boolean getDoorOpen() {
		return doorOpen;
	}

	public void setDoorOpen(Boolean doorOpen) {
		this.doorOpen = doorOpen;
	}

	public Float getDirection() {
		return direction;
	}

	public void setDirection(Float direction) {
		this.direction = direction;
	}

	public Float getVoltage() {
		return voltage;
	}

	public void setVoltage(Float voltage) {
		this.voltage = voltage;
	}

	public Boolean getInternalAlert() {
		return internalAlert;
	}

	public void setInternalAlert(Boolean internalAlert) {
		this.internalAlert = internalAlert;
	}

	public Boolean getPanicAlert() {
		return panicAlert;
	}

	public void setPanicAlert(Boolean panicAlert) {
		this.panicAlert = panicAlert;
	}

	public Float getSpeed() {
		return speed;
	}

	public void setSpeed(Float speed) {
		this.speed = speed;
	}


	public Float getOdometer() {
		return odometer;
	}

	public void setOdometer(Float odometer) {
		this.odometer = odometer;
	}

	public Integer getLifeTimer() {
		return lifeTimer;
	}

	public void setLifeTimer(Integer lifeTimer) {
		this.lifeTimer = lifeTimer;
	}

	public Float getIgnitionTimeHour() {
		return ignitionTimeHour;
	}

	public void setIgnitionTimeHour(Float ignitionTimeHour) {
		this.ignitionTimeHour = ignitionTimeHour;
	}

	public Boolean getPaneAlert() {
		return paneAlert;
	}

	public void setPaneAlert(Boolean paneAlert) {
		this.paneAlert = paneAlert;
	}

	public Boolean getIgnition() {
		return ignition;
	}

	public void setIgnition(Boolean ignition) {
		this.ignition = ignition;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Float getTemperature() {
		return temperature;
	}

	public void setTemperature(Float temperature) {
		this.temperature = temperature;
	}

	public Double getCalculatedSpeed() {
		return calculatedSpeed;
	}

	public void setCalculatedSpeed(Double calculatedSpeed) {
		this.calculatedSpeed = calculatedSpeed;
	}

	public Long getRoletteCounter() {
		return roletteCounter;
	}

	public void setRoletteCounter(Long roletteCounter) {
		this.roletteCounter = roletteCounter;
	}

	public Boolean getGprsOnline() {
		return gprsOnline;
	}

	public void setGprsOnline(Boolean gprsOnline) {
		this.gprsOnline = gprsOnline;
	}

	public List<String> getUrls() {
		return urls;
	}

	public void setUrls(List<String> urls) {
		this.urls = urls;
	}

	public PatternType getPatternTypeEnumValue() {
		if(patternType != null){
			return PatternType.values()[patternType];
		}
		return PatternType.OUTGOING;
	}

	public Integer getPatternType() {
		return patternType;
	}
	
	public void setPatternType(Integer patternType) {
		this.patternType = patternType;
	}

	public Double getPatternFraction() {
		return patternFraction;
	}

	public void setPatternFraction(Double patternFraction) {
		this.patternFraction = patternFraction;
	}

	
}
