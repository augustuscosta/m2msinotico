package br.com.m2msolutions.sinotico.model;

import java.io.Serializable;

public class BusService implements Serializable{

	private static final long serialVersionUID = -4996679990269391230L;

	private Long busServiceId;

	private String description;

	private String shortName;
	
	private String name;
	
	private String controlPointName;

	private Long controlPointCode;
	
	private String destinationName;
	
	private Long destinationId;
	
	private String codeNextVehicle;
	
	private Integer arrivalNextTime;
	
	private String returnDestinationName;
	
	private Long returnDestinationId;
	
	private String returnCodeNextVehicle;
	
	private Integer returnArrivalNextTime;
	
	private Double length;
	
	private Double outgoingLength;
	
	private Double returningLength;

	
	
	public Double getOutgoingLength() {
		return outgoingLength;
	}

	public void setOutgoingLength(Double outgoingLength) {
		this.outgoingLength = outgoingLength;
	}

	public Double getReturningLength() {
		return returningLength;
	}

	public void setReturningLength(Double returningLength) {
		this.returningLength = returningLength;
	}

	public Long getBusServiceId() {
		return busServiceId;
	}

	public void setBusServiceId(Long busServiceId) {
		this.busServiceId = busServiceId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getControlPointName() {
		return controlPointName;
	}

	public void setControlPointName(String controlPointName) {
		this.controlPointName = controlPointName;
	}

	public Long getControlPointCode() {
		return controlPointCode;
	}

	public void setControlPointCode(Long controlPointCode) {
		this.controlPointCode = controlPointCode;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public Long getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(Long destinationId) {
		this.destinationId = destinationId;
	}

	public String getCodeNextVehicle() {
		return codeNextVehicle;
	}

	public void setCodeNextVehicle(String codeNextVehicle) {
		this.codeNextVehicle = codeNextVehicle;
	}

	public Integer getArrivalNextTime() {
		return arrivalNextTime;
	}

	public void setArrivalNextTime(Integer arrivalNextTime) {
		this.arrivalNextTime = arrivalNextTime;
	}

	public String getReturnDestinationName() {
		return returnDestinationName;
	}

	public void setReturnDestinationName(String returnDestinationName) {
		this.returnDestinationName = returnDestinationName;
	}

	public Long getReturnDestinationId() {
		return returnDestinationId;
	}

	public void setReturnDestinationId(Long returnDestinationId) {
		this.returnDestinationId = returnDestinationId;
	}

	public String getReturnCodeNextVehicle() {
		return returnCodeNextVehicle;
	}

	public void setReturnCodeNextVehicle(String returnCodeNextVehicle) {
		this.returnCodeNextVehicle = returnCodeNextVehicle;
	}

	public Integer getReturnArrivalNextTime() {
		return returnArrivalNextTime;
	}

	public void setReturnArrivalNextTime(Integer returnArrivalNextTime) {
		this.returnArrivalNextTime = returnArrivalNextTime;
	}

	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}
	
	
}
